require("ggplot2")
require("reshape2")

# save_png(p, filename, h, w)
save_png = function(p, filename, h, w) {
  png(filename = filename, height = h, width = w)
  print(p)
  dev.off()
  return(p)
}

## Useful constants
savedir <- file.path("C:", "Users", "jhu", "Documents", "Box Sync",
                     "Gartnerlab Data", "Individual Folders", "Jennifer Hu",
                     "Data", "Western")
summaryfile <- file.path(savedir, "2018-10 summary.csv")
exptdate <- "2018-10-24"

## Open saved summary data
western_data <- read.table(summaryfile, header = TRUE, sep=",")

western_data <- melt(western_data, id.vars = 1:4,
	measure.vars = names(western_data)[5:14],
	variable.name = "Protein", value.name = "Value")

western_data$Date <- exptdate
western_data$Starve <- 5 + as.numeric(western_data$Media == "-")
western_data$Stim <- as.numeric(western_data$Media == "+")

# control (GFP)
GFPs <- western_data[western_data$Transgene == "GFP", ]
conditions <- c("Batch", "Drug", "Protein", "Date", "Starve", "Stim")
n <- nrow(western_data)
western_data$FoldGFP <- NA

for (i in 1:n) {
  condition <- western_data[i, conditions]
  for (j in 1:nrow(GFPs)) {
    if (all(GFPs[j, conditions] == condition)) {
      break
    }
  }
  western_data$FoldGFP[i] <- western_data$Value[i] / GFPs$Value[j]
}

# order
western_data$Transgene <- factor(western_data$Transgene,
	levels = c("GFP", "PTENsh", "Ecadsh", "PIK3CA", "KRAS", "Her2"))
western_data$Drug <- factor(western_data$Drug,
	levels = c("DMSO", "TFA", "serabelisib", "copanlisib", "afuresertib",
		"CHIR-99021", "NSC23766", "GSK-2334470"))
western_data$Protein <- factor(western_data$Protein,
	levels = c("GFP", "Her2", "PIK3CA", "pAkt1", "p.mTOR",
		"p.PDK1", "KRAS", "p.B.Raf", "p.ERK", "b.catenin"))

# big overview of all data, batched

print(save_png(p = ggplot(data = western_data[western_data$Media == "+",],
                          aes(x=Drug, y=Value, fill=Drug)) +
                 geom_col(position="dodge") + ggtitle("Stimulated") +
                 facet_grid(Protein ~ Transgene, scales="free_y", space="free_x") +
                 theme_minimal() +
                 theme(axis.text.x = element_text(angle = 90, hjust = 1)),
               filename = file.path(savedir, "2018-10 summary.png"),
               h = 1000, w = 1400))
print(save_png(p = ggplot(data = western_data[western_data$Media == "-",],
                          aes(x=Drug, y=Value, fill=Drug)) +
                 geom_col(position="dodge") + ggtitle("Stimulated") +
                 facet_grid(Protein ~ Transgene, scales="free_y", space="free_x") +
                 theme_minimal() +
                 theme(axis.text.x = element_text(angle = 90, hjust = 1)),
               filename = file.path(savedir, "2018-10 summary.png"),
               h = 1000, w = 1400))

# things express what they"re supposed to
p <- ggplot(data = western_data[
		western_data$Protein %in% c("GFP", "Her2", "PIK3CA", "KRAS") &
		western_data$Batch==2,], aes(x=Transgene, y=Value, fill=Drug)) +
	 geom_col(position="dodge") +
	 facet_grid(Protein ~ Media, scales="free") +
	 theme_minimal()
print(p)

# afuresertib effects
p <- ggplot(data = western_data[western_data$Drug %in% c("DMSO", "afuresertib") &
		western_data$Batch==1 &
		western_data$Protein %in% c("GFP", "PIK3CA", "pAkt1", "p.mTOR", "p.PDK1"),],
		aes(x=Transgene, y=Value, fill=Drug)) +
	 geom_col(position="dodge") +
	 facet_grid(Protein ~ Media, scales="free") +
	 theme_minimal()
print(p)

# b.catenin and p.B.Raf
p <- ggplot(data = western_data[
		western_data$Protein %in% c("b.catenin", "p.B.Raf") &
		western_data$Batch==1,], aes(x=Transgene, y=Value, fill=Drug)) +
	 geom_col(position="dodge") +
	 facet_grid(Protein ~ Media, scales="free") +
	 theme_minimal()
print(p)

# PIK3CA and p-Akt1
p <- ggplot(data = western_data[
		western_data$Protein %in% c("PIK3CA", "pAkt1") &
		western_data$Drug != "afuresertib" &
		western_data$Batch==1,], aes(x=Transgene, y=Value, fill=Drug)) +
	 geom_col(position="dodge") +
	 facet_grid(Protein ~ Media, scales="free") +
	 theme_minimal()
print(p)

# and p-PDK1
p <- ggplot(data = western_data[
		western_data$Protein %in% c("PIK3CA", "pAkt1", "p.PDK1") &
		western_data$Drug != "afuresertib" &
		western_data$Batch==2,], aes(x=Transgene, y=Value, fill=Drug)) +
	 geom_col(position="dodge") +
	 facet_grid(Protein ~ Media, scales="free") +
	 theme_minimal()
print(p)

# source("plot_western.r")