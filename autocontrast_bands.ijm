/*   2018-10-29
 *   Jennifer Hu
 *   Select a directory containing subfolder /ROIs (tifs)
 *   Batch opens all tifs, inverts, auto-adjusts contrast, and saves as PNG and tif
 *   in /Bands.
 */

 dir_source = getDirectory("Choose Source Directory ");

 // images won't actually open, saving time
setBatchMode(true);
list = getFileList(dir_source + "ROIs/");
for (i=0; i<list.length; i++) {
	img = list[i];
	open(dir_source + "ROIs/" + img);
	// process the image
	run("Invert");
	run("Enhance Contrast", "saturated=0.35");
	run("Apply LUT"); // saves color corrections
	// save
	saveAs("TIF", dir_source + "Bands/" + img);
	saveAs("PNG", dir_source + "Bands/" + File.nameWithoutExtension + ".png");
	close();
}
